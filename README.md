# CircleCI Build Image #

The image has OpenJDK, Node and NPM installed.

## Build ##
To build just run
`docker build . -t buildimage`

## Docker Hub ##
The image is built and publicly accessible via DockerHub.
